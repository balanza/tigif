/**
 * createGifView 
 * Metodo factory per la creazione di un elemento UI.GifView
 * Si tratta di un'estensione di Ti.UI.ImageView. Le API prevedono la presenza di n file immagine che si chiamano come il file base più l'indice del frame.
 * Esempio: per la gif `/images/esempio.gif` serviranno i file `/images/esempio0.gif`, `/images/esempio2.gif`, `/images/esempio3.gif`, etc...
 * I parametri aggiuntivi/modificati rispetto a ImageView per il funzionamento con gif animate sono:
 *
 *     - `image`: nome base della gif da eseguire (quindi `esempio.gif`)
 *
 *     - `delays`: array con i millisecondi di permanenza per ogni frame
 *
 *     - `autoplay`: se true, l'animazione parte in automatico. Default `false`
 *
 * Il resto dei parametri sono gli stessi di ImageView
 *     
 * @param  {object} params hash dei parametri
 * @return {GifView} oggetto GifView
 */
exports.createGifView = function(params) {
    //parametri specifici della gif
    var gif_params = _(params).pick('image', 'delays', 'autoplay');
    //parametri generici dell'image view
    var img_params = _(params).omit(_(gif_params).keys());
    console.log('gif_params', gif_params);

    //creo l'oggetto image view base
    var imageView = Ti.UI.createImageView(img_params);

    //reference del timer
    var timerId;
    //pointer del frame
    var frameIndex = 0;

    //formatta il file name in base all'indice corrente
    function getImageName(index) {
        var i = gif_params.image.replace(/\.(?=[^.]*$)/, index + ".");
        //  console.log('getImageName', gif_params.image, index, i);
        return i;
    }

    //visualizza il frame relativo all'indice corrente
    function showFrame(index) {
        imageView.image = getImageName(index);
    }

    //incrementa il puntatore e mostra il prossimo frame
    function next() {
        frameIndex = (frameIndex + 1) % gif_params.delays.length;
        showFrame(frameIndex);
    }


    //blocca l'animazione e ritorna al primo frame
    function reset() {
        pause();
        frameIndex = 0;
        showFrame(frameIndex);

    };

    //blocca l'animazine
    function pause() {

        clearTimeout(timerId);
        timerId = null;
    };

    //avvia l'animazione
    function play() {


        (function run() {
            var delay = gif_params.delays[frameIndex];
            //  console.log('run', delay, frameIndex);
            timerId = setTimeout(function() {
                next();
                run();
            }, delay);
        })();

    };

    //toggle tra `pause` e `start`
    function toggle() {  
        var isPlaying = !!timerId;
        if (isPlaying) {
            pause();
        } else {

            play();
        }
    }


    //public api
    imageView.play = play;
    imageView.pause = pause;
    imageView.reset = reset;
    imageView.toggle = toggle;

    //mostro il primo frame
    showFrame(frameIndex);
    //avvia l'animazione
    if (gif_params.autoplay) play();



    //ritorna l'oggetto ui dell'immagine
    return imageView;

}